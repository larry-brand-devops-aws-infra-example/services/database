# Database docker image

## How to run locally:
Execute command `docker-compose up` with [docker-compose-file](https://gitlab.com/larry-brand-devops-aws-infra-example/ci-cd/kubernetes/-/blob/master/local/docker-compose.yml)

or execute command

`$docker run --name example-app-database -itd -p 27017:27017 -e "MONGO_INITDB_ROOT_USERNAME=root" -e "MONGO_INITDB_ROOT_PASSWORD"="pass123!" larrybrand/devops-aws-infra-example-database`

## How to run in k8s cluster:
Open port 27017 and pass environment variables "MONGO_INITDB_ROOT_USERNAME=root" , "MONGO_INITDB_ROOT_PASSWORD"="pass123!"
