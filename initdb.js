// initdb.js
// ===================================================

//use admin
db = db.getSiblingDB("admin");
db.createUser({
    user: "tchallenge_user",
    pwd: "tchallenge_password",
    roles: [{ role: "readWrite", db: "hello_db" }]
})

//use hello_db
db = db.getSiblingDB("hello_db");
db.createCollection("orders")

db.orders.insert([{
    "_id": ObjectId("5eb92873d35c2d9398b5d8a4"),
    "email": "user@user.com",
    "callnumbers": ["911, 02"]
},
{
    "_id": ObjectId("5eb92873d35c2d9398b5d8a1"),
    "email": "user2@user2.com",
    "callnumbers": ["+79111234567, +49 30 901820"]
}])
